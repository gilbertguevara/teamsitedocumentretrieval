package com.hugeinc.lambda

import com.amazonaws.services.lambda.runtime.Context
import com.amazonaws.services.sns.AmazonSNSAsyncClient
import com.amazonaws.services.sns.AmazonSNSClient
import com.hugeinc.model.Environment
import com.hugeinc.model.TeamsiteDocument
import com.hugeinc.model.TeamsiteRequest
import groovy.time.TimeCategory

import java.text.SimpleDateFormat

/**
 * User: HUGE-gilbert
 * Date: 1/21/16
 * Time: 9:17 AM
 */
class TeamsiteRetrieval {
    private static final String TEAMSITE_METADATA_URL_SLUG = 'TeamSite/Metadata/url_slug'
    private static final String TEAMSITE_METADATA_TYPE = 'TeamSite/Templating/DCR/Type'
    private static final String SNS_CAPTURE_ARN = 'arn:aws:sns:us-east-1:136652547359:capture-topic'

    private AmazonSNSClient amazonSNSClient = new AmazonSNSAsyncClient()

    /**
     * Lambda handler for Teamsite retrieval.
     * <br/>
     * Retrieves last updated teamsite documents and publish a message with the corresponding url to a SNS topic
     * @param teamsiteRequest TeamsiteRequest containing the environment and the maximum number of documents to get
     * @param context Lambda context
     * @return A list containing the full url on scion site, the original teamsite path and the type of document
     */
    List<TeamsiteDocument> handleRequest(TeamsiteRequest teamsiteRequest, Context context) {
        if (teamsiteRequest?.max > 100) {
            context.logger.log('Teamsite document retrieval is restricted to 100 documents')
            return new ArrayList<TeamsiteDocument>()
        }

        Environment environment = Environment.valueOf(teamsiteRequest?.environment)
        String server = environment?.lscsServer
        def from = use (TimeCategory) {
            // Defaulting to a month ago
            Date dateFrom = new Date() - 1.month
            new SimpleDateFormat('MM/dd/yyyy').format(dateFrom)
        }
        String project = environment.project
        String max = String.valueOf(teamsiteRequest?.max)

        String teamsiteDocumentsXml = "http://scion-lscs-${server}.elasticbeanstalk.com/v1/document\$?q=(lastModifiedDate:>=$from)&start=0&max=$max&project=Scion_Redesign/$project".toURL().text
        def results = new XmlSlurper().parseText(teamsiteDocumentsXml)
        def fieldMetadataClosure = { field, fieldName ->
            field.@name == fieldName
        }

        def teamsiteDocuments = results.assets.document.findAll{ document->
            document.metadata.field.find { field-> fieldMetadataClosure(field, TEAMSITE_METADATA_URL_SLUG) }
        }.collect { document->
            println("Document found: " + document.@path)
            def urlSlugField = document.metadata.field.find { field-> fieldMetadataClosure(field, TEAMSITE_METADATA_URL_SLUG)}
            def typeField = document.metadata.field.find { field-> fieldMetadataClosure(field, TEAMSITE_METADATA_TYPE)}

            // Publish a message to Amazon SNS
            String url = environment.serverHost + urlSlugField.text()
            amazonSNSClient.publish(SNS_CAPTURE_ARN, url)

            new TeamsiteDocument(teamsitePath: document.@path, url: url, teamsiteType: typeField.text())
        }

        teamsiteDocuments
    }
}
