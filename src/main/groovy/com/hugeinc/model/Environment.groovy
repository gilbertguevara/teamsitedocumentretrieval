package com.hugeinc.model

/**
 * User: HUGE-gilbert
 * Date: 1/21/16
 * Time: 9:46 AM
 */
enum Environment {
    DEVELOPMENT('dev1', 'development', 'https://scion-user:chang3it@tst1.aws.scion.com'),
    QA('tst1', 'qa', 'https://scion-user:chang3it@dev1.aws.scion.com'),
    AUTHORING('tst1', 'preview', 'https://scion-user:chang3it@dev2.aws.scion.com'),
    STAGING('staging', 'staging', 'https://scion-user:chang3it@staging.scion.com'),
    PRODUCTION('production', 'production', 'https://www.scion.com')

    private static final String CREDENTIALS = 'scion-user:chang3it'

    String lscsServer
    String project
    String serverHost

    Environment(String lscsServer, String project, String serverHost) {
        this.lscsServer = lscsServer
        this.project = project
        this.serverHost = serverHost
    }
}
