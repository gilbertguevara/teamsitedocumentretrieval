package com.hugeinc.model

/**
 * User: HUGE-gilbert
 * Date: 1/21/16
 * Time: 9:28 AM
 */
class TeamsiteDocument implements Serializable {
    String teamsitePath
    String url
    String teamsiteType
}